#include <stdio.h>

#define LEN 1000000000

int main(){
    float max = -1, min = LEN;
    int i;

    for(i = 0; i < LEN; ++i)
    {
        float aux = (i - LEN/2.0);
        aux = (aux < 0)? -1 * aux : aux;

        max = (max < aux)? aux : max;
        min = (min > aux)? aux : min;
    }

    printf("RESULTADOS\n");
    printf("------------------------------------\n");
    printf("Maior: %lf\n", max);
    printf("Menor: %lf\n", min);

    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#define LEN 1000000000

int main(int argc, char **argv){
    int me, total;
    int tag_max = 1, tag_min = 2;

    MPI_Init(&argc, &argv);
    int start, end;
    int i;
    /* Defines the variables that will store the highest value found and the
       lowest value found, they are initialized with the smallest and highest
       possible value */
    float max = -1, min = LEN;
    
    /* Stores the process id and total processes */
    MPI_Comm_rank(MPI_COMM_WORLD, &me);
	MPI_Comm_size(MPI_COMM_WORLD, &total);

    /* Eliminating process 0 of total workers */
    total--;

    if (!total) {
        printf("É necessário que existam pelo menos 2 processos\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    /* It separates the actions of the zero process of the others */
    if(me){
        /* Calculates the range that the current process should*/
        start = (LEN / total) * (me - 1);
        end = (me == total) ? LEN : (LEN / total) + start;

        printf("ME: %d --- start: %d, end: %d\n",me, start, end);

        /* Calculates the value of all indexes of this */
        for(i = start; i < end; ++i)
    	{
            float aux = (i - LEN/2.0);
    		aux = (aux < 0)? -1 * aux : aux;

    		max = (max < aux)? aux : max;
    		min = (min > aux)? aux : min;
    	}

        /* Sending maximun and minimum values to the process zero */
        MPI_Send(&max, 1, MPI_FLOAT, 0, tag_max, MPI_COMM_WORLD);
        MPI_Send(&min, 1, MPI_FLOAT, 0, tag_min, MPI_COMM_WORLD);

        printf("ME: %d --- Acabei de calcular -----Max: %lf, Min: %lf\n", me, max, min);

    } else{
        /* Receive maximum and minimum values from all others process */
        for (i = 1; i <= total; i++) {
            float rec_max, rec_min;

            MPI_Status status;
            MPI_Recv(&rec_max, 1, MPI_FLOAT, i, tag_max, MPI_COMM_WORLD, &status);
            MPI_Recv(&rec_min, 1, MPI_FLOAT, i, tag_min, MPI_COMM_WORLD, &status);

            /* Stores the highest and lowest value */
            max = (max < rec_max)? rec_max : max;
    		min = (min > rec_min)? rec_min : min;
            printf("Master recebeu de: %d -----Max: %lf, Min: %lf\n", i, rec_max, rec_min);
        }

        /* Show highest and lowest value */
        printf("RESULTADOS\n");
		printf("------------------------------------\n");
		printf("Maior: %lf\n", max);
		printf("Menor: %lf\n", min);
    }

    MPI_Finalize();
    return 0;
}

# Experimento utilizando MPI feito na disciplina de Fundamentos de Sistemas Operacionais

## Integrantes
* 14/0080619 Camila Carneiro Ferrer Santos
* 14/0066543 Vinicius Pinheiro da Silva Correa

## Execução da aplicação

### Pré requisitos
Ter instalado o framework MPI.

Para instalação: [Link](http://mpitutorial.com/tutorials/installing-mpich2//)

### Compilação
Para compilar o código utilizando o MPI execute o comando:
```
mpicc mpi.c -o prog
```

### Execução

#### Um unico host
Para executar o código utilizando o MPI execute o comando:
```
mpirun -n <numero de processos> prog
```

#### Em mais que um host
Para a configuração das máquinas virtuais utilizou-se, como base, o tutorial [Running an MPI Cluster within a LAN](http://mpitutorial.com/tutorials/running-an-mpi-cluster-within-a-lan/), sendo que alguns passos foram executados de forma diferente. No geral o processo seguido foi:

1) Instalação do open-ssh nas maquinas;

2) Configuração do ssh para login via rsa;

3) Instalação do NFS e configuração da pasta compartilhada;

4) Copiar o executável (gerado com o comando mpicc) para a pasta compartilhada;

5) Criar arquivo com todos os hosts que executarão o codigo (hostfile);

6) Execução do programa.

```
mpiexec -hostfile <file> -n <num process> <executavel>
```

No tutorial apontado, ele explica detalhadamente os passos aqui feitos. Neste tutorial foi encotrado alguns problemas nos passo a passo, então baseado nele seguimos os passos acima.

### Possíveis problemas
#### Falha na conexão com o host
Para solucionar o problema, tente verificar se via ping as duas maquinas se enxergam. Caso não se enxerguem, verifique a configuração da rede.

Uma outra causa é o arquivo hostfile, verifique se em cada linha tem SOMENTE o número de IP das maquinas da rede e confirme se todas estão alcançaveis via ping e via ssh.

#### Falha na autenticação
Caso consigam, verifique se o login via ssh está funcionando com a autenticação rsa s/ senha.

Verifique se o `ssh-agent` esta sendo executado nas duas maquinas.
